<?php

namespace Tests\Unit\Models;

use App\Models\WordProcessor;
use App\Models\GeneratorOptions;
use App\Models\WordRepository as WordRepository;
use App\Services\PasswordGeneratorService;
use App\Services\SystemService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class WordRepositoryEmptyDatabaseExceptionTest extends TestCase
{
  /**
   * @var PasswordGeneratorService
   */
  private $passwordGeneratorService;

  /**
   * Setup.
   *
   * @return void
   */
  protected function setUp(): void
  {
    parent::setUp();

    Storage::disk('local')->put('testing/database.sqlite', '');
    App::make(SystemService::class)->importFilesToDb(subPath: 'testing/exceptions/empty-database-exception');

    $wordProcessor = new WordProcessor();
    $wordRepository = new WordRepository();
    $this->passwordGeneratorService = new PasswordGeneratorService($wordProcessor, $wordRepository);
  }

  /**
   * @test
   */
  public function wordRepositoryEmptyDatabaseException()
  {
    $options = new GeneratorOptions();
    $passwordGeneratorResult = $this->passwordGeneratorService->generate($options);

    $this->assertEquals(true, $passwordGeneratorResult->hasError());
  }

}