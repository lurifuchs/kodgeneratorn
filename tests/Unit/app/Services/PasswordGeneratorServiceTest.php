<?php

namespace Tests\Unit\Models;

use App\Helpers\StringAnalyserHelper;
use App\Models\WordProcessor;
use App\Models\WordRepository;
use App\Models\GeneratorOptions;
use App\Services\PasswordGeneratorService;
use App\Services\SystemService;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class PasswordGeneratorServiceTest extends TestCase
{
  /**
   * @var PasswordGeneratorService
   */
  private $passwordGeneratorService;

  /**
   * Setup.
   *
   * @return void
   */
  protected function setUp(): void
  {
    parent::setUp();

    Storage::disk('local')->put('testing/database.sqlite', '');
    App::make(SystemService::class)->importFilesToDb();

    $wordProcessor = new WordProcessor();
    $wordRepository = new WordRepository();
    $this->passwordGeneratorService = new PasswordGeneratorService($wordProcessor, $wordRepository);
  }

    /**
   * @test
   */
  public function generate()
  {
    $options = new GeneratorOptions();
    $string = $this->passwordGeneratorService->generate($options);
    $this->assertTrue(!empty($string));
  }

  /**
   * @test
   */
  public function generateWithCapitalWords()
  {
    $options = new GeneratorOptions([
      'number_of_words' => 2,
      'capitalize' => 'word'
    ]);
    $passwordGeneratorResult = $this->passwordGeneratorService->generate($options);
    $numberOfCaptitalLetters = StringAnalyserHelper::stringDifference(mb_strtolower($passwordGeneratorResult->getPassword()), $passwordGeneratorResult->getPassword());
    $this->assertEquals(2, $numberOfCaptitalLetters);
    $this->assertEquals(2, $passwordGeneratorResult->getSourceWords()->count());
  }

  /**
   * @test
   */
  public function generateWithCapitalFirst()
  {
    $options = new GeneratorOptions([
      'number_of_words' => 2,
      'capitalize' => 'first'
    ]);
    $passwordGeneratorResult = $this->passwordGeneratorService->generate($options);
    $password = $passwordGeneratorResult->getPassword();
    $this->assertEquals($password, ucfirst($password));
  }

  /**
   * @test
   */
  public function generateWithCapitalLast()
  {
    $options = new GeneratorOptions([
      'number_of_words' => 2,
      'capitalize' => 'last'
    ]);
    $passwordGeneratorResult = $this->passwordGeneratorService->generate($options);
    $password = $passwordGeneratorResult->getPassword();

    $uclast = strrev(ucfirst(strrev($password)));
    $this->assertEquals($password, $uclast);
  }

  /**
   * @test
   */
  public function generateWithNumbers()
  {
    $options = new GeneratorOptions([
      'numbers' => 'start',
      'number_of_numbers' => 4,
    ]);
    $passwordGeneratorResult = $this->passwordGeneratorService->generate($options);
    $matches = preg_replace('/[^0-9]+/', '', $passwordGeneratorResult->getPassword());
    $this->assertEquals(4, strlen($matches));
  }

  /**
   * @test
   */
  public function generateWithNumbersBetween()
  {
    $options = new GeneratorOptions([
      'numbers' => 'between',
      'number_of_numbers' => 5,
    ]);
    $passwordGeneratorResult = $this->passwordGeneratorService->generate($options);
    $password = $passwordGeneratorResult->getPassword();
    $matches = preg_replace('/[^0-9]+/', '', $password);
    $this->assertEquals(5, strlen($matches));
    $this->assertTrue((bool) preg_match('/[a-zåäöA-ZÅÄÖ](\d+)[a-zåäöA-ZÅÄÖ]/i', $password));
}

  /**
   * @test
   */
  public function generateWithSpecialChars()
  {
    $options = new GeneratorOptions([
      'special_chars' => 'end',
      'number_of_special_chars' => 3,
    ]);
    $passwordGeneratorResult = $this->passwordGeneratorService->generate($options);
    $matches = StringAnalyserHelper::hasSpecialChars($passwordGeneratorResult->getPassword());
    $this->assertEquals(3, $matches);
  }

  /**
   * @test
   */
  public function generateWithSpecialCharsBetween()
  {
    $options = new GeneratorOptions([
      'number_of_words' => 4,
      'special_chars' => 'between',
      'number_of_special_chars' => 2,
    ]);
    $passwordGeneratorResult = $this->passwordGeneratorService->generate($options);
    $password = $passwordGeneratorResult->getPassword();
    $matches = StringAnalyserHelper::hasSpecialChars($password);
    $this->assertEquals(2, $matches);
    $this->assertTrue((bool) preg_match('/[a-zåäöA-ZÅÄÖ0-9]([^a-zåäöA-ZÅÄÖ0-9]+)[a-zåäöA-ZÅÄÖ0-9]/i', $password));
  }

  /**
   * @test
   */
  public function generateWithoutSwedishChars()
  {
    $options = new GeneratorOptions([
      'use_swedish_chars' => false,
    ]);
    $passwordGeneratorResult = $this->passwordGeneratorService->generate($options);
    $matches = StringAnalyserHelper::hasSwedishChars($passwordGeneratorResult->getPassword());
    $this->assertEquals(0, $matches);
  }

}
