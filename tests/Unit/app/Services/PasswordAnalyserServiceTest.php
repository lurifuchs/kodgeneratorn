<?php

namespace Tests\Unit\Models;

use App\Livewire\PasswordAnalyser;
use App\Models\GeneratorOptions;
use App\Models\WordProcessor;
use App\Models\WordRepository;
use App\Services\PasswordAnalyserService;
use App\Services\PasswordGeneratorService;
use App\Services\SystemService;
use Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use InvalidArgumentException;
use Tests\TestCase;

class PasswordAnalyserServiceTest extends TestCase
{
  /**
   * @var PasswordGeneratorService
   */
  private $passwordGeneratorService;

  /**
   * @var PasswordAnalyserService
   */
  private $passwordAnalyserService;

  /**
   * Setup.
   *
   * @return void
   */
  protected function setUp(): void
  {
    parent::setUp();

    Storage::disk('local')->put('testing/database.sqlite', '');
    App::make(SystemService::class)->importFilesToDb();

    $wordProcessor = new WordProcessor();
    $wordRepository = new WordRepository();
    $this->passwordGeneratorService = new PasswordGeneratorService($wordProcessor, $wordRepository);
    $this->passwordAnalyserService = new PasswordAnalyserService();
  }

  /**
   * @test
   */
  public function analyseResultStructure()
  {
    $options = new GeneratorOptions();
    $passwordGeneratorResult = $this->passwordGeneratorService->generate($options);
    $analysisResult = $this->passwordAnalyserService->analyse($passwordGeneratorResult->getPassword(), $options);

    $this->assertTrue(is_numeric($analysisResult->getLength()));
    $this->assertTrue(is_object($analysisResult->getStrengthExplanations()));
    $this->assertTrue(is_object($analysisResult->getDescription()));

    $required = ['header', 'body'];
    $countIntersect = count(array_intersect_key(array_flip($required), get_object_vars($analysisResult->getDescription())));
    $this->assertEquals(count($required), $countIntersect);

    $this->assertTrue(is_object($analysisResult->getZxcvbn()));
    $this->assertTrue(is_object($analysisResult->getEntropy()));
    $this->assertTrue(is_object($analysisResult->getHibp()));
    $this->assertTrue(!empty($analysisResult->getScore()));
  }

  /**
   * @test
   */
  public function analyseBreachedPassword()
  {
    // Test a breached password
    $options = new GeneratorOptions();
    $analysisResult = $this->passwordAnalyserService->analyse('password', $options);
    $this->assertTrue(isset($analysisResult->getHibp()->breached));
  }

  /**
   * @test
   */
  public function analyseSingleWordPassword()
  {
    // Test one word passwords
    $options = new GeneratorOptions([
      'number_of_words' => 1,
      'special_chars' => 'null',
      'numbers' => 'null',
    ]);
    $passwordGeneratorResult = $this->passwordGeneratorService->generate($options);

    // Fake HIBP request.
    Http::fake([
      'https://api.pwnedpasswords.com/*' => Http::response('HIBP test response', 400)
    ]);

    $analysis = $this->passwordAnalyserService->analyse($passwordGeneratorResult->getPassword(), $options);
    $this->assertLessThan(2, $analysis->getScore());
    $this->assertIsString($analysis->getHibp()->error);
  }

  /**
   * @test
   */
  public function analyseEmptyPassword()
  {
    $this->expectException(InvalidArgumentException::class);

    $options = new GeneratorOptions();
    $this->passwordAnalyserService->analyse(new PasswordAnalyser(), $options);
  }

  /**
   * @test
   */
  public function getPool()
  {
    $pool = $this->passwordAnalyserService->getPool('abc');
    $this->assertEquals(29, $pool);

    $pool = $this->passwordAnalyserService->getPool('abcDEF');
    $this->assertEquals(55, $pool);

    $pool = $this->passwordAnalyserService->getPool('abc123');
    $this->assertEquals(39, $pool);

    $pool = $this->passwordAnalyserService->getPool('abcDEF123');
    $this->assertEquals(65, $pool);

    $pool = $this->passwordAnalyserService->getPool('abcDEF123!');
    $this->assertEquals(65 + strlen(config('kodgeneratorn.special_chars')), $pool);
  }
}
