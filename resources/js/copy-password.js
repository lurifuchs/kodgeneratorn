import ClipboardJS from 'clipboard';

document.addEventListener('DOMContentLoaded', function() {
  const copyPasswordButtons = document.querySelectorAll('.button--copy');

  console.log(ClipboardJS.isSupported());
  if (ClipboardJS.isSupported() == false) {
    copyPasswordButtons.forEach(e => e.remove());
  } else {
    const clipboard = new ClipboardJS(copyPasswordButtons);

    clipboard.on('success', function (e) {
      showNotification('Lösenordet kopierades till urklipp');
      e.clearSelection();
    });

    clipboard.on('error', function (e) {
      showNotification('Lösenordet kunde inte kopieras');
    });
  }

  /**
   * Show notifiction.
  */
  function showNotification(message) {
    let notification = document.getElementById('notification-template').cloneNode(true);
    notification.innerHTML = message;
    notification.style.display = 'block';
    notification.setAttribute('aria-live', 'assertive')
    document.body.appendChild(notification);

    setTimeout(function() {
      notification.style.display = 'none';
    }, 5000);
  }
});