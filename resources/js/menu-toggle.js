document.addEventListener('DOMContentLoaded', function() {
  const header = document.querySelector('header');
  let button = document.getElementById('nav-toggle-button');

  // Menu
  button.addEventListener('click', function(event) {
    // Curreny menu state
    const ariaExpanded = button.getAttribute('aria-expanded') == 'true' ? true : false;

    // Change menu state
    button.setAttribute('aria-expanded', ariaExpanded ? 'false' : 'true');
    button.setAttribute('aria-label', ariaExpanded ? 'Öppna meny' : 'Stäng meny');
    button.closest('header').classList.toggle('nav-open');
    document.body.classList.toggle('disable-scroll');
    trapFocus(header);
  });

  function trapFocus(element) {
    var focusableEls = element.querySelectorAll('a:not(.main-content-link), button');
    var firstFocusableEl = focusableEls[0];
    var lastFocusableEl = focusableEls[focusableEls.length - 1];
    var KEYCODE_TAB = 9;

    element.addEventListener('keydown', function(e) {
      var isTabPressed = (e.key === 'Tab' || e.keyCode === KEYCODE_TAB);

      if (!isTabPressed) {
        return;
      }

      if (e.shiftKey) {
        if (document.activeElement === firstFocusableEl) {
          lastFocusableEl.focus();
          e.preventDefault();
        }
      } else {
        if (document.activeElement === lastFocusableEl) {
          firstFocusableEl.focus();
          e.preventDefault();
        }
      }
    });
  }
});