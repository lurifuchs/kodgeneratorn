<!DOCTYPE html>
<html lang="sv">
<head>
  <title>{{ $title }}</title>
  <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
  <link rel="manifest" href="/images/favicon/site.webmanifest">
  <link rel="mask-icon" href="/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" type="image/jpg" href="/images/favicon/favicon-16x16.png"/>
  <link rel="stylesheet" href="{{ mix('dist/main.css') }}">
  <link rel="canonical" href="{{ Request::url() }}">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="{{ $description }}" />
  <meta property="og:url" content="https://kodgeneratorn.se" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="{{ $title }}" />
  <meta property="og:description" content="{{ $description }}" />
  <meta property="og:image" content="https://kodgeneratorn.se/images/social.webp" />

  <script src="/dist/main.min.js"></script>
</head>

<body>
  <a href="#main" class="sr-only main-content-link">Hoppa till huvudinnehållet</a>
  <header>
    <div class="center">
      <a href="/" class="logo-link header-3">Kodgeneratorn</a>
      <button id="nav-toggle-button" aria-label="Öppna menyn" aria-controls="main-menu"></button>
    </div>

    <nav id="main-menu">
      <ul>
        <li><a href="/">Skapa ett lösenord</a></li>
        <li><a href="{{ route('what_is_a_strong_password') }}">Vad är ett starkt lösenord?</a></li>
        <li><a href="{{ route('how_are_the_passwords_created') }}">Hur skapas lösenorden?</a></li>
        <li><a href="{{ route('what_is_security') }}">Vad är säkerhet?</a></li>
        <li><a href="{{ route('test_your_password') }}">Testa ditt lösenord</a></li>
        <li><a href="{{ route('integrations_and_plugins') }}">Integrationer och plugins</a></li>
        <li><a href="{{ route('api_documentation') }}">API-dokumentation</a></li>
      </ul>
    </nav>
  </header>

  <main id="main">
    @yield('content')
  </main>

  <footer>
    <div class="center footer-content">
      <div class="image">
        <img src="/images/davidfuchs.webp" height="200" width="200" alt="En bild på David Fuchs">
      </div>
      <div class="legible">
        <div class="header-2">Hej, jag heter David!</div>
        <p>På dagarna jobbar jag som webbutvecklare på <a href="https://040.se" target="_blank">040.se</a> och på kvällar och helger kodar jag <a href="https://localfoodnodes.org/sv" target="_blank">localfoodnodes.org</a> eller spelar gitarr med <a href="https://sagorsomledermotslutet.se" target="_blank">Sagor Som Leder Mot Slutet</a>. Vill du komma i kontakt med mig är du välkommen att maila mig på <a href="mailto:kodgeneratorn@lurifuchs.se">kodgeneratorn@lurifuchs.se</a>.</p>
        <p>Om du vill kika på koden för kodgeneratorn så finns den på <a href="https://gitlab.com/lurifuchs/kodgeneratorn" target="_blank">GitLab</a>.</p>
      </div>
    </div>
    <div class="footer-legal legible">
      <div class="center">
        <a href="{{ route('development') }}">Version #20240808</a>
      </div>
    </div>
  </footer>

  <div id="notification-template" class="notification"></div>
  @livewireScripts
</body>
</html>
