@assets
  <script src="/dist/copy-password.min.js"></script>
@endassets

@script
  <script>
    const copyButton = document.querySelector('.button--copy');
    const passwordField = document.querySelector('input.password');

    /**
     *  Select password on focus.
     */
    passwordField.addEventListener('focus', () => passwordField.select());
  </script>
@endscript

<div class="password-generator-container">
  <div class="center">
    @if ($passwordError)
      <div class="password-generator">
        {{ $passwordError }}
      </div>
    @else
      <div class="password-generator">
        <div class="password-field">
          <button class="button button--inline-big" title="Generera ett nytt lösenord" aria-label="Generera ett nytt lösenord" wire:click="generateNewPassword">Generera</button>

          {{-- Spaceless  --}}
          <input type="text" value="{{ $this->password }}" readonly wire:loading.class="password-loading" class="password" id="password" role="alert" aria-live="assertive" aria-label="{{ $this->password }}. Lösenordets styrka är {{ $this->analysePassword($this->password)->getDescription()->header }}. {{ $this->analysePassword($this->password)->getDescription()->body }}">

          <button class="button button--round button--copy" data-clipboard-target="#password" title="Kopiera lösenordet till urklipp" aria-label="Kopiera lösenordet till urklipp">Kopiera lösenordet</button>
        </div>

        <div class="buttons">
          <button class="button button--mobile" title="Generera ett nytt lösenord" aria-label="Generera ett nytt lösenord" wire:click="generateNewPassword">Generera lösenord</button>
          <button class="button button--copy button--mobile" data-clipboard-target="#password" title="Kopiera lösenordet till urklipp" aria-label="Kopiera lösenordet till urklipp">Kopiera lösenordet</button>
        </div>

        <div class="password-info-and-options">
          <div class="password-info">
            {{-- <div class="password-info__arrow"></div> --}}
            <div class="password-info__content">
              <div id="strength-summary" class="strength-summary">
                <strong>
                  {{ $this->analysePassword($this->password)->getDescription()->header }}
                </strong>
                <p>
                  {{ $this->analysePassword($this->password)->getDescription()->body }}
                </p>
              </div>
            </div>
          </div>

          <div class="options">

            {{-- Number of words  --}}
            <div class="option">
              <label for="number-of-words">Antal ord</label>
              <select id="number-of-words" wire:model.live="numberOfWords">
                @for ($i = 1; $i <= 5; $i++)
                  <option value="{{ $i }}">{{ $i }} ord</option>
                @endfor
              </select>
            </div>

            {{-- Capitalize  --}}
            <div class="option">
              <label class="label" for="capitalize">Versaler</label>
              <select id="capitalize" wire:model.live="capitalize">
                <option value="null">Inga versaler</option>
                <option value="first">I början</option>
                <option value="last">I slutet</option>
                <option value="word">Varje ord</option>
                <option value="random">Slumpmässigt</option>
              </select>
            </div>

            {{-- Numbers --}}
            <div class="option">
              <label class="label" for="numbers">Siffror</label>
              <select id="numbers" wire:model.live="numbers">
                <option value="null">Inga siffror</option>
                <option value="start">I början</option>
                <option value="end">I slutet</option>
                <option value="between">Mellan ord</option>
                <option value="random">Slumpmässigt</option>
              </select>
            </div>

            <div class="option">
              <label class="label" for="number-of-numbers">Antal siffror</label>
              <select id="number-of-numbers" wire:model.live="numberOfNumbers" {{ $numbers == 'null' ? 'disabled' : '' }}>
                @for ($i = 1; $i <= 9; $i++)
                  <option value="{{ $i }}">{{ $i }} {{ $i == 1 ? 'siffra' : 'siffror' }}</option>
                @endfor
              </select>
            </div>

            {{-- Special chars --}}
            <div class="option">
              <label class="label" for="special_chars">Specialtecken</label>
              <select id="special_chars" wire:model.live="specialChars">
                <option value="null">Inga specialtecken</option>
                <option value="start">I början</option>
                <option value="end">I slutet</option>
                <option value="between">Mellan ord</option>
                <option value="random">Slumpmässigt</option>
              </select>
            </div>

            <div class="option">
              <label class="label" for="number-of-special-chars">Antal specialtecken</label>
              <select id="number-of-special-chars" wire:model.live="numberOfSpecialChars" {{ $specialChars == 'null' ? 'disabled' : '' }}>
                @for ($i = 1; $i <= 9; $i++)
                  <option value="{{ $i }}">{{ $i }} specialtecken</option>
                @endfor
              </select>
            </div>

            {{-- Swedish chars --}}
            <div class="option checkbox">
              <div id="swedish-chars-label" class="label">Svenska tecken</div>
              <input id="swedish-chars" type="checkbox" wire:model.live="swedishChars">
              <label
                for="swedish-chars"
                tabindex="0"
                role="checkbox"
                aria-checked="{{ $swedishChars ? 'true' : 'false' }}"
                aria-label="Inkludera svenska tecken"
                x-data="{ isFocused: false }"
                x-on:focus="isFocused = true"
                x-on:blur="isFocused = false"
                x-bind:class="{ 'checkbox-focus-visible': isFocused }"
              >Inkludera</label>
            </div>
          </div> {{-- end options --}}
        </div>

      </div>
    @endif
  </div>
</div>