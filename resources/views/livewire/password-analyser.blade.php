<div class="password-generator-container">
  <div class="center">
    <div class="password-generator">
      <div class="password-field">
        <button class="button button--inline-big" title="Analysera ditt lösenord" aria-label="Analysera ditt lösenord" wire:click="analysePassword">Analysera</button>

        <input class="password" type="text" wire:model="password" placeholder="Skriv in lösenordet du vill testa">
      </div>

      <div class="buttons">
        <button class="button button--mobile" wire:click="analysePassword" aria-label="Analysera lösenordet">Analysera lösenordet</button>
      </div>

      <div class="password-info">
        <div class="password-info__arrow"></div>

        @if ($this->error)
          <div class="password-info__content">
            {{ $this->error }}
          </div>
        @elseif ($this->passwordAnalysisResult)
          <div class="password-info__content">
            <div class="strength-summary"><strong>{{ $this->passwordAnalysisResult->getDescription()->header }}</strong><p>{{ $this->passwordAnalysisResult->getDescription()->body }}</p></div>
          </div>
        @else
          <div class="password-info__content">Fyll i ditt lösenord här ovanför och klicka på analysera för att se vilken poäng det får.</div>
        @endif

      </div>

      </div>
    </div>
  </div>
</div>