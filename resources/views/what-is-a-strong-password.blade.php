@extends('layout', [
  'title' => 'Vad är definitionen av ett starkt lösenord?',
  'description' => 'Att använda starka lösenord är viktigt för din säkerhet, men vad är ett säkert lösenord? Här går vi igenom det lite snabbt.'
])

@section('content')
  <section>
    <div class="center legible">
      <h1>Vad är ett starkt lösenord?</h1>
      <blockquote>
        <p>Lösenord är våra nycklar på internet, det är så vi identifierar oss på olika webbsidor. Att skydda dina lösenord är avgörande för att förhindra att någon obehörig kan få tillgång till din information och dina användarkonton. Med starka lösenord och ett smart användande kommer du långt.
        <p>Starka lösenord kan skapas på många sätt. Grundregeln är att ju fler tecken ett lösenord har desto säkrare är det. För att lättare  kunna komma ihåg ett lösenord med exempelvis 12 tecken så använder sig många av så kallade lösenordsfraser. Man tar då exempelvis första bokstaven i varje ord i en mening, fras eller sångtext och skapar lösenordet.</p>
        <p>Om du varierar tecknen genom att använda små och stora bokstäver, siffror och specialtecken skapar du ett ännu säkrare lösenord och minskar därmed risken avsevärt att någon obehörig ska knäcka lösenordet.</p>
      </blockquote>

      <p>
        Texten ovan kommer från <a href="https://www.msb.se/sv/rad-till-privatpersoner/informationssakerhet/sakra-dina-losenord/">MSB</a> och är en lös beskrivning av vad ett starkt lösenord är, men nu ska vi bli mer tekniska. Kodgeneratorn tar hänsyn till flera olika parametrar för att generera och bedömma ett lösenords säkerhet.
      </p>

      <h2>Entropi</h2>
      <p>
        Ett sätt att bedömma om ett lösenord är säkert baserat på dess längd och typ av tecken är att räkna ut entropin. Wikipedia beskriver entropis som "ett mått på sannolikheten för att ett system skall inta ett visst tillstånd". Om vi tolkar den meningen till denna kontext så innebär entropin sannolikheten att ett system ska kunna gissa sig fram till lösenordet, det som också kallas <a href="https://sv.wikipedia.org/wiki/Brute_force" target="_blank">brute force</a>.
      </p>
      <p>
        Lösenordsentropi uttrycks i termer av bitar. Ett lösenords entropi kan beräknas genom att hitta entropin per tecken, vilket är en loggbas 2 av antalet tecken i teckenuppsättningen som används, multiplicerat med antalet tecken.

        <ul>
          <li>Ett lösenord som redan är känt har 0 bitars entropi.</li>
          <li>Ett lösenord som kräver som mest två gissningar för att hitta har 1 bitars entropi.</li>
          <li>Ett lösenord med <i>n</i> bitars entropi skulle kräva 2^n gissningar för att garantera att lösenordet hittas.</li>
        </ul>
      </p>

      <p>
        Kodgeneratorn använder formeln E = log2(R<span class="raised">L</span>) för att räkna ut entropin, där R är antalet tecken i teckenuppsättningen och L är längden på lösenordet. Här kan vi alltså se tydligt att längden på lösenordet spelar stor roll, men också vilken mängd tecken som vi utgår ifrån när vi genererar lösenordet. Därför är det bra att blanda in siffror och specialtecken i ditt lösenord.
      </p>

      <h2>Det räcker inte med entropi</h2>
      <p>
        Ett problem med svaga lösenord är att de ofta är baserade på vanliga ord eller meningar som finns i ordböcker som används för att gissa sig fram till lösenord. Varje år kommer det rapporter med de vanligaste lösenorden, så ett lösenord som till exempel "password1234" som faktiskt är 12 tecken långt är inte svårgissat eftersom det består av ett vanligt ord, och siffrorna 1234 är ett vanligt mönster som är återkommande hos lösenord.
      </p>

      <p>
        Det är även viktigt att inte använda ord eller siffror som är relaterade till dig eller till tjänsten du skapar lösenord till. Undvik att t.ex. använda din adress, siffror i ditt personnummer, namn eller liknande. Vid en läcka så kan den typen av uppgifter komma ut och gör då ditt lösenord svagt. Det är därför det är bra att generera lösenord med slumpmässigt valda ord, siffror och tecken som inte har med något att göra men som fortfarande är lätt att komma ihåg.
      </p>

      <p>Läs mer om <a href="/hur-skapas-losenorden">hur lösenorden skapas</a> eller om <a href="/vad-ar-sakerhet">säkerhet generellt på internet</a></p>
    </div>
  </section>
@endsection