@extends('layout', [
  'title' => 'API-dokumentation',
  'description' => 'API-dokumentation för dig som är utvecklare så du kan använda kodgeneratorn för att skapa lösenord i dina projekt.'
])

@section('content')
  <section>
    <div class="center legible">
      <h1>API-dokumentation</h1>
      <p>Så, hur använder du API:et? Enkelt, gör bara ett GET-anrop till https://kodgeneratorn.se/api/generate (eller besök url:en via browsern) för att generera lösenord.</p>
      <pre><code>
    GET: https://kodgeneratorn.se/api/generate

    {
      "number_of_words": int,
      "capitalize": false|"word"|"random",
      "numbers": false|"start"|"end"|"random",
      "number_of_numbers": int,
      "special_chars": false|"start"|"end"|"between"|"random",
      "number_of_special_chars": int,
      "swedish_chars": bool
    }
      </code></pre>

      <p>Responsen från API:et ser ut som nedan.</p>

      <pre>
        <code>
  {!! $apiResponse !!}
        </code>
      </pre>

      <p>Om något inte är tydligt, eller om du har förslag på förbättringar så är det bara att du hör av dig.</p>
    </div>
  </section>
@endsection