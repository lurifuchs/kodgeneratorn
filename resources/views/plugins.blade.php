@extends('layout', [
  'title' => 'Integrationer och plugins till ramverk',
  'description' => 'Kodgeneratorn är en lösenordsgenerator som hjälper dig att skapa lösenord med svenska ord. Det gör det lätt för dig att komma ihåg dina starka lösenord.'
])

@section('content')
  <section>
    <div class="center legible">
      <h1>Integrationer och plugins till ramverk</h1>
      <p class="preamble">Kodgeneratorn är en lösenordsgenerator som hjälper dig att skapa starka lösenord med svenska ord. Genom att generera lösenord med slumpmässigt valda ord som sätts ihop till en mening blir det lätt för dig att komma ihåg dina lösenord.</p>

      <h2>WordPress</h2>
      <p>Om du vill använda kodgeneratorn tillsammans med WordPress kan du kopiera och klistra in koden nedan i functions.php.</p>

        <pre>
          <code>
  add_filter( 'random_password', 'kodgeneratorn_random_password');

  function kodgeneratorn_random_password() {
    $params =  [
      'number_of_words' => 4,
      'capitalize' => 'word',
      'numbers' => 'start',
      'number_of_numbers' => 1,
      'special_chars' => 'end',
      'number_of_special_chars' => 1,
      'swedish_chars' => 1
    ];
    $url = 'https://kodgeneratorn.se/api/generate';
    $response = wp_remote_get($url . '?' . http_build_query( $params ));

    if ( is_wp_error( $response ) ) {
      // Generate a fallback password here if kodgeneratorn is down.
    } else {
      $body = json_decode($response['body']);
      return $body->generated;
    }
  }
          </code>
        </pre>
    </div>
  </section>
@endsection
