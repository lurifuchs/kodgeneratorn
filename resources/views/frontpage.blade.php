@extends('layout', [
  'title' => 'Skapa lösenord med kodgeneratorn',
  'description' => 'Kodgeneratorn är en lösenordsgenerator som hjälper dig att skapa lösenord med svenska ord. Det gör det lätt för dig att komma ihåg dina starka lösenord.'
])

@section('content')
  <section>
    <div class="center legible">
      <h1>Skapa lösenord - starka och lätta att komma ihåg</h1>
      <p class="preamble">Kodgeneratorn är en lösenordsgenerator som hjälper dig att skapa starka lösenord med svenska ord. Genom att generera lösenord med slumpmässigt valda ord som sätts ihop till en mening blir det lätt för dig att komma ihåg dina lösenord.</p>
    </div>
  </section>

  <section class="password-generator-section">
    <livewire:password-generator key="generator" />
  </section>

  <section>
    <div class="center legible">
      <h2>Vad är ett starkt lösenord?</h2>
      <p>Lösenord är våra nycklar på internet, det är så vi identifierar oss på olika webbsidor. Att skydda dina lösenord är avgörande för att förhindra att någon obehörig kan få tillgång till din information och dina användarkonton. Med starka lösenord och ett smart användande kommer du långt.
      <p>Starka lösenord kan skapas på många sätt. Grundregeln är att ju fler tecken ett lösenord har desto säkrare är det. För att lättare  kunna komma ihåg ett lösenord med exempelvis 12 tecken så använder sig många av så kallade lösenordsfraser. Man tar då exempelvis första bokstaven i varje ord i en mening, fras eller sångtext och skapar lösenordet.</p>
      <p>Om du varierar tecknen genom att använda små och stora bokstäver, siffror och specialtecken skapar du ett ännu säkrare lösenord och minskar därmed risken avsevärt att någon obehörig ska knäcka lösenordet.</p>
      <p><a href="/vad-ar-ett-starkt-losenord">Läs mer om att skapa starka lösenord</a></p>
    </div>
  </section>

  <section>
    <div class="center legible">
      <h2>Hur skapas lösenorden?</h2>
      <p>Kodgeneratorn använder svenska ord för att skapa unika fraser som är lätta att komma ihåg. Fraserna baseras på olika ordklasser. I dagsläget finns det {{ $verbsCount }} verb, {{ $adjectivesCount }} adjektiv och {{ $nounsCount }} substantiv, det blir riktigt många unika lösenord som kan skapas. Om du väljer att skapa ett lösenord med endast ett ord använder jag en tabell som innehåller {{ $wordsCount }} unika ord.</p>

      <p>
        Styrkan på lösenorden räknas ut med hjälp av <a href="https://dropbox.tech/security/zxcvbn-realistic-password-strength-estimation" target="_blank">zxcvbn</a> och en entropiuträkning.
        zxcvbn gör ett bra jobb att analysera återkommande mönster hos lösenord och jämför även lösenord mot en engelsk ordbok, men eftersom kodgeneratorn använder svenska ord med svenska tecken används även en egen entropiuträkning som tar hänsyn till det.
        Jag har även gjort några egna antaganden som att lösenord med endast ett ord alltid är lättgissat.
        Lösenorden kollas även mot <a href="https://haveibeenpwned.com" target="_blank">haveibeenpwned</a> för att upptäcka om det har förekommit i någon läcka. I så fall visas en varning.
      </p>

      <p><a href="/hur-skapas-losenorden">Läs mer om hur lösenorden skapas</a></p>
    </div>
  </section>

  <section class="divider">
    <div class="center legible">
      <h2>Viktigt!</h2>
      <p>Kodgeneratorn sparar inga av de skapade lösenorden. Orden slumpas fram från den stora mängden som finns i databasen, tillsammans med inställningarna som också (i alla fall vissa av dem) är slumpmässiga. Kom ihåg att spara de lösenord du gillar, för när du klickar på knappen och skapar ett nytt lösenord är det förra borta!</p>
      <p>Du är även helt anonym här på kodgeneratorn.se, här finns inga cookies som spårar dig. Jag använder <a href="https://matomo.org/">Matomo</a> i utvärderingssyfte för att se antalet besökare samt antalet genererade och testade lösenord. Tjänsten är open source så du kan själv <a href="https://gitlab.com/lurifuchs/kodgeneratorn" target="_blank">kika på koden</a> för att vara säker på att du är säker.</p>
    </div>
  </section>

  <section>
    <div class="center legible">
      <h2>Utvecklare?</h2>
      <p>Vi utvecklare gillar ju API:er så därför har jag dokumenterat hur du kan använda kodgeneratorn för att skapa lösenord till dina tjänster. Eller så kan du ta koden från <a href="#">GitLab</a> och göra din egen grej.<p>
      <pre><code>
  GET: https://kodgeneratorn.se/api/generate

  {
    "number_of_words": int,
    "capitalize": false|"word"|"random",
    "numbers": false|"start"|"end"|"random",
    "number_of_numbers": int,
    "special_chars": false|"start"|"end"|"between"|"random",
    "number_of_special_chars": int,
    "swedish_chars": bool
  }
      </code></pre>

      <p><a href="/api-dokumentation">API-dokumentation</a></p>
    </div>
  </section>
@endsection
