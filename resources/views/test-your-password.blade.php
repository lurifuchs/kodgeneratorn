@extends('layout', [
  'title' => 'Skapa lösenord med kodgeneratorn',
  'description' => 'Testa styrkan på ditt lösenord med Kodgeneratorn. Lösenordet körs genom samma analys som de genererade lösenorden och får en poäng beroende på hur starka de är.'
])

@section('content')
  <section>
    <div class="center legible">
      <h1>Testa styrkan på ditt lösenord</h1>
      <p class="preamble">Kodgeneratorn är en lösenordsgenerator som hjälper dig att skapa starka lösenordsfraser med svenska ord. Men Kodgeneratorn kan också testa styrkan på ditt lösenord för att du ska få en uppfattning om hur starkt det är. Om ditt lösenord är svagt kan du enkelt <a href="/">skapa ett nytt starkt lösenord här</a>.</p>
    </div>
  </section>

  <section class="password-generator-section">
    <livewire:password-analyser />
  </section>

  <section>
    <div class="center legible">
      <h2>Viktigt!</h2>
      <p>Kodgeneratorn sparar inga av de analyserade lösenorden. Ditt lösenord körs endast genom samma uträkning som för de genererade lösenorden för att kunna sätta ett betyg.</p>
      <p>Du är helt anonym här på kodgeneratorn.se, här finns inga cookies som spårar dig. Tjänsten är open source så du kan själv <a href="https://gitlab.com/lurifuchs/kodgeneratorn" target="_blank">kika på koden</a> för att vara säker på att du är säker.</p>
    </div>
  </section>

  <section>
    <div class="center legible">
      <h2>Vad är ett starkt lösenord?</h2>
      <p>Lösenord är våra nycklar på internet, det är så vi identifierar oss på olika webbsidor. Att skydda dina lösenord är avgörande för att förhindra att någon obehörig kan få tillgång till din information och dina användarkonton. Med starka lösenord och ett smart användande kommer du långt.
      <p>Starka lösenord kan skapas på många sätt. Grundregeln är att ju fler tecken ett lösenord har desto säkrare är det. För att lättare  kunna komma ihåg ett lösenord med exempelvis 12 tecken så använder sig många av så kallade lösenordsfraser. <a href="/">Generera en stark lösenordsfras som är lätt att komma ihåg här</a>.</p>
      <p>Om du varierar tecknen genom att använda små och stora bokstäver, siffror och specialtecken skapar du ett ännu säkrare lösenord och minskar därmed risken avsevärt att någon obehörig ska knäcka lösenordet.</p>
      <p><a href="/vad-ar-ett-starkt-losenord">Läs mer om att skapa starka lösenord</a></p>
    </div>
  </section>
@endsection
