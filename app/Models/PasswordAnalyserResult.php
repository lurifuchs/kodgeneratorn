<?php

namespace App\Models;

use Exception;
use Illuminate\Support\Collection;

class PasswordAnalyserResult
{
  /**
   * All the password strength descriptions.
   *
   * @var array
   */
  private static $strengthExplanations = [
    0 => [
      'header' => 'Svagt och väldigt lättgissat!',
      'body' => 'Det finns med stor sannolikhet i ordböcker som används vid ordboksattacker. Styrkan är %d/4 och entropin är %d bitar.'
    ],
    1 => [
      'header' => 'Lättgissat!',
      'body' => 'Liknande lösenord finns i ordböcker som används vid ordboksattacker. Styrkan är %d/4 och entropin är %d bitar.'
    ],
    2 => [
      'header' => 'Gissningsbart!',
      'body' => 'Ger visst skydd vid ohämmade attacker. Styrkan är %d/4 och entropin är %d bitar.'
    ],
    3 => [
      'header' => 'Omöjligt att gissa!',
      'body' => 'Erbjuder måttligt skydd från offline slow-hash-scenarion. Styrkan är %d/4 och entropin är %d bitar.'
    ],
    4 => [
      'header' => 'Galet starkt!',
      'body' => 'Omöjligt att gissa och ger ett starkt skydd mot offline slow-hash scenarion. Styrkan är %d/4 och entropin är %d bitar.'
    ],
    'breached' => [
      'header' => 'Varning!',
      'body' => 'Det här lösenordet har dykt upp i läckor %d gånger'
    ]
  ];

  /**
   * Constructor.
   *
   * @param string $password
   * @param Collection $sourceWords
   */
  public function __construct(
    private int $length,
    private int $score,
    private array $description,
    private array $zxcvbn,
    private array $entropy,
    private array $hibp
  ) { }

  /**
   * Static helper function.
   *
   * @param integer|string $strength
   * @return object
   */
  public static function getDescriptionFromStrength(int|string $strength): object
  {
    return (object) self::$strengthExplanations[$strength];
  }

  /**
   * Get password length.
   *
   * @return int
   */
  public function getLength(): int
  {
    return $this->length;
  }

  /**
   * Get password score.
   *
   * @return int
   */
  public function getScore(): int
  {
    return $this->score;
  }

  /**
   * Get the description for this analysis.
   *
   * @return object
   */
  public function getDescription(): object
  {
    return (object) $this->description;
  }

  /**
   * Get zxcvbn data.
   *
   * @return object
   */
  public function getZxcvbn(): object
  {
    return (object) $this->zxcvbn;
  }

  /**
   * Get the entropy for this analysis.
   *
   * @return object
   */
  public function getEntropy(): object
  {
    return (object) $this->entropy;
  }

  /**
   * Get breach info from Have I Been Pwned.
   *
   * @return object
   */
  public function getHibp(): object
  {
    return (object) $this->hibp;
  }

  /**
   * Get all the generic strenght explanations available.
   *
   * @return object
   */
  public function getStrengthExplanations(): object
  {
    return (object) self::$strengthExplanations;
  }

  /**
   * Convert object to array.
   *
   * @return array
   */
  public function toArray(): array
  {
    return [
      'length' => $this->getLength(),
      'strength_explanations' => $this->getStrengthExplanations(),
      'zxcvbn' => $this->getZxcvbn(),
      'entropy' => $this->getEntropy(),
      'breached' => $this->getHibp(),
      'strength_explanation' => $this->getDescription()
    ];
  }
}