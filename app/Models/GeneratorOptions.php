<?php

namespace App\Models;

use Illuminate\Http\Request;
use stdClass;

class GeneratorOptions
{
  /**
   * @var bool
   */
  private $capitalize;

  /**
   * @var int
   */
  private $numberOfWords;

  /**
   * @var bool
   */
  private $specialChars;

  /**
   * @var int
   */
  private $numberOfSpecialChars;

  /**
   * @var bool
   */
  private $numbers;

  /**
   * @var int
   */
  private $numberOfNumbers;

  /**
   * @var bool
   */
  private $useSwedishChars;

  /**
   * Constructor.
   *
   * @param stdClass|Request $options
   */
  public function __construct(array|stdClass|Request $options = null)
  {
    if (is_array($options)) {
      $options = (object) $options;
    }

    if ($options) {
      $this->capitalize = $options->capitalize ?? null;
      $this->numberOfWords = $options->number_of_words ?? null;
      $this->numbers = $options->numbers ?? false;
      $this->numberOfNumbers = $options->number_of_numbers ?? null;
      $this->specialChars = $options->special_chars ?? false;
      $this->numberOfSpecialChars = $options->number_of_special_chars ?? null;
      $this->useSwedishChars = $options->use_swedish_chars ?? null;
    }
  }

  /**
   * Capitalize password in some different ways.
   * Options are: null, word or random
   *
   * @return string
   */
  public function useCapitalize(): string
  {
    return $this->capitalize ?? 'word';
  }

  /**
   * Get the number of words that make up the password.
   *
   * @return integer
   */
  public function getNumberOfWords(): int
  {
    return $this->numberOfWords ?? 3;
  }

  /**
   * Add numbers to the password in different ways.
   * Options are: null, start, end, random.
   *
   * @return string
   */
  public function numbers(): string
  {
    return $this->numbers ?? 'end';
  }

  /**
   * Check if number should be used depending on the options.
   *
   * @return bool
   */
  public function useNumbers(): bool
  {
    return $this->numbers != 'null';
  }

  /**
   * Get the number of numbers.
   *
   * @return integer
   */
  public function getNumberOfNumbers(): int
  {
    if ($this->useNumbers()) {
      return $this->numberOfNumbers ?? 1;
    } else {
      return $this->numberOfNumbers ?? 0;
    }
  }

  /**
   * Add special chars to the password in different ways.
   * Options are: null, start, end, between, random.
   *
   * @return string
   */
  public function specialChars(): string
  {
    return $this->specialChars ?? 'start';
  }

  /**
   * Check if special chars should be used depending on the options.
   *
   * @return bool
   */
  public function useSpecialChars(): bool
  {
    return $this->specialChars != 'null';
  }

  /**
   * Get number of special chars to be added to the password.
   *
   * @return integer
   */
  public function getNumberOfSpecialChars(): int
  {
    if ($this->useSpecialChars()) {
      return  $this->numberOfSpecialChars ?? 1;
    } else {
      return $this->numberOfSpecialChars ?? 0;
    }
  }

  /**
   * Use Swedish words in the password.
   *
   * @return boolean
   */
  public function useSwedishChars(): bool
  {
    return $this->useSwedishChars ?? true;
  }

}