<?php

namespace App\Livewire;

use App\Models\GeneratorOptions;
use App\Services\PasswordAnalyserService;
use App\Services\PasswordGeneratorService;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\App;
use Livewire\Attributes\Computed;
use Livewire\Component;

class PasswordGenerator extends Component
{
  // Models - filter options
  public string $capitalize = 'word';
  public string $numbers = 'end'; // start, end, random
  public int $numberOfNumbers = 1;
  public int $numberOfSpecialChars = 1;
  public string $specialChars = 'start'; // start, end, between, random
  public int $numberOfWords = 3;
  public bool $swedishChars = true;

  // Variables
  private $passwordGeneratorResult;

  private $passwordError;

  /**
   * Mount
   *
   * @return void
   */
  public function mount()
  {
    $this->passwordGeneratorResult = App::make(PasswordGeneratorService::class)->generate($this->getOptions());
  }

  /**
   * Generate password.
   *
   * @return PasswordGeneratorResult
   */
  #[Computed]
  public function password()
  {
    $this->passwordError = $this->passwordGeneratorResult->getError();

    return $this->passwordGeneratorResult->getPassword();
  }

  /**
   * Analyse password.
   *
   * @param string $password
   * @return PasswordAnalysisResult
   */
  #[Computed]
  public function analysePassword($password)
  {
    $passwordAnalysisResult = App::make(PasswordAnalyserService::class)->analyse(
      $password,
      $this->getOptions()
    );

    return $passwordAnalysisResult;
  }

  /**
   * Get filter options.
   *
   * @return GeneratorOptions
   */
  private function getOptions(): GeneratorOptions
  {
    $options = [
      'capitalize' => $this->capitalize,
      'number_of_words' => $this->numberOfWords,
      'numbers' => $this->numbers,
      'number_of_numbers' => $this->numberOfNumbers,
      'special_chars' => $this->specialChars,
      'number_of_special_chars' => $this->numberOfSpecialChars,
      'use_swedish_chars' => $this->swedishChars,
    ];

    return new GeneratorOptions((object) $options);
  }

  /**
   * Number of words updated.
   *
   * @return void
   */
  public function updated()
  {
    $this->passwordGeneratorResult = App::make(PasswordGeneratorService::class)->generate($this->getOptions());
  }

  /**
   * Undocumented function
   *
   * @return void
   */
  public function generateNewPassword()
  {
    $this->passwordGeneratorResult = App::make(PasswordGeneratorService::class)->generate($this->getOptions());
  }

  /**
   * Render.
   *
   * @return View
   */
  public function render(): View
  {
    return view('livewire.password-generator', [
      'passwordError' => $this->passwordError
    ]);
  }
}