<?php

namespace App\Services;

use App\Models\GeneratorOptions;
use App\Models\PasswordGeneratorResult;
use App\Models\WordProcessor;
use App\Models\WordRepository;
use Exception;
use Illuminate\Http\Request;

class PasswordGeneratorService
{
  /**
   * Constructor.
   *
   * @param WordProcessor $wordProcessor
   * @param WordRepository $wordRepository
   */
  public function __construct(
    private WordProcessor $wordProcessor,
    private WordRepository $wordRepository
  ) { }

  /**
   * Generate password.
   *
   * @param WordRepository $wordRepository
   * @param Request $request
   * @return PasswordGeneratorResult
   */
  public function generate(GeneratorOptions $options): PasswordGeneratorResult
  {
    // Calculate the shortest length of word
    $wordLength = 12;
    $wordLength -= $options->getNumberOfNumbers() - $options->getNumberOfSpecialChars();

    $numberOfWords = $options->getNumberOfWords();

    // Get words from DB
    try {
      $words = $this->wordRepository->get($numberOfWords, $wordLength);
    } catch (Exception $e) {
      return new PasswordGeneratorResult(null, null, $e);
    }

    $originalWords = clone $words;

    // Remove Swedish chars
    if (!$options->useSwedishChars()) {
      $words = $this->wordProcessor->removeSwedishCharsFromCollection($words);
    }

    // Generate!
    $generated = $words->join('');

    // Static order: capitalize
    $generated = $this->wordProcessor->capitalize($words, $generated, $options->useCapitalize(), $numberOfWords);

    $randomOrderFunctions = [];

    // Random order: special chars between words
    if ($options->specialChars() == 'between') {
      $randomOrderFunctions['specialCharsBetweenWords'] = [$words, $numberOfWords, $options->getNumberOfSpecialChars()];
    }

    // Random order: number between words
    if ($options->numbers() == 'between') {
      $randomOrderFunctions['numbersBetweenWords'] = [$words, $numberOfWords, $options->getNumberOfNumbers()];
    }

    // Random order: special chars (start, end, random)
    if ($options->specialChars() != 'between') {
      $randomOrderFunctions['specialChars'] = [$options->specialChars(), $options->getNumberOfSpecialChars()];
    }

    // Random order: numbers (start, end, random)
    $randomOrderFunctions['numbers'] = [$options->numbers(), $options->getNumberOfNumbers()];

    uksort($randomOrderFunctions, function() { return rand() > rand(); });

    if (config('app.env') == 'testing') {
      $functionOrder = join('|', array_keys($randomOrderFunctions));
      // print "\nFunction order: \n\e[0;32m$functionOrder\e[0m\n";
      // print "\nGenerated password: \n\e[0;32m$generated\e[0m\n";
    }

    if (!empty($randomOrderFunctions)) {
      foreach ($randomOrderFunctions as $function => $parameters) {
        array_unshift($parameters, $generated);
        $generated = $this->wordProcessor->$function(...$parameters);
      }
    }

    // Generation is done. If you want to test with a really weak password uncomment the line below.
    // $generated = 'password';

    if (config('app.env') == 'testing') {
      // print "\e[0;32m$generated\e[0m\n";
    }

    return new PasswordGeneratorResult($generated, $originalWords);
  }
}