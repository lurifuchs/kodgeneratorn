<?php

namespace App\Services;

use App\Models\PasswordAnalyserResult;
use App\Models\PasswordGeneratorResult;

class ApiService
{
  /**
   * Api v1
   *
   * @param PasswordGeneratorResult $passwordGeneratorResult
   * @param PasswordAnalyserResult $passwordAnalyserResult
   * @return void
   */
  public function getV1ResponseData(
    PasswordGeneratorResult $passwordGeneratorResult,
    PasswordAnalyserResult $passwordAnalyserResult
  ) {
    return [
      'source' => $passwordGeneratorResult->getSourceWords(),
      'generated' => $passwordGeneratorResult->getPassword(),
      'analysis' => [
        'length' => $passwordAnalyserResult->getLength(),
        'strength_explanations' => $passwordAnalyserResult->getStrengthExplanations(),
        'zxcvbn' => $passwordAnalyserResult->getZxcvbn(),
        'entropy' => $passwordAnalyserResult->getEntropy(),
        'breached' => $passwordAnalyserResult->getHibp(),
        'strength_explanation' => $passwordAnalyserResult->getDescription(),
      ]
    ];
  }

}