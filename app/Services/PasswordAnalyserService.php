<?php

namespace App\Services;

use App\Helpers\StringAnalyserHelper;
use App\Livewire\PasswordAnalyser;
use App\Models\GeneratorOptions;
use App\Models\PasswordAnalyserResult;
use Exception;
use Illuminate\Support\Facades\Http;
use InvalidArgumentException;
use Throwable;
use ZxcvbnPhp\Zxcvbn;

// https://github.com/bjeavons/zxcvbn-php
// https://www.omnicalculator.com/other/password-entropy

class PasswordAnalyserService
{
  /**
   * Analyse the strength of a password.
   *
   * @param $password allow mixed values but validate in method and thow InvalidArgumentException instead
   * @param PasswordGeneratorOptions $options
   * @return PasswordAnalyserResult
   * @throws Exception
   */
  public function analyse($password, GeneratorOptions $options = null): PasswordAnalyserResult
  {
    try {
      if (!$password) {
        throw new InvalidArgumentException('Ange ett lösenord.');
      }

      $password = (string) $password; // Throwing Throwable

      // Password length
      $length = strlen($password);

      // zxcvbn
      $zxcvbn = new Zxcvbn();
      $zxcvbn = $zxcvbn->passwordStrength($password);

      // Entropy
      $entropy = $this->calculateEntropy($password);

      // Compute combined score
      $score = floor(($zxcvbn['score'] + $entropy['score']) / 2);

      // Check HIBP
      $hibp = $this->getHIBPData($password);

      // Exceptions that lower score.
      if ($hibp['breached']) {
        $score = 0;
        $description = PasswordAnalyserResult::getDescriptionFromStrength('breached');
        $descriptionHeader = $description->header;
        $descriptionBody = sprintf($description->body, $hibp['count']);
      } else {
        if ($options && $options->getNumberOfWords() == 1 && !$options->useSpecialChars() && !$options->useNumbers()) {
          // The use of one word is always easy to guess
          $score = 1;
        }

        $description = PasswordAnalyserResult::getDescriptionFromStrength($score);
        $descriptionHeader = $description->header;
        $descriptionBody = sprintf($description->body, $score, $entropy['value']);
      }

      $description = [
        'header' => $descriptionHeader,
        'body' => $descriptionBody,
      ];

      return new PasswordAnalyserResult(
        length: $length,
        score: $score,
        description: $description,
        zxcvbn: $zxcvbn,
        entropy: $entropy,
        hibp: $hibp
      );
    } catch (Exception $e) {
      throw new InvalidArgumentException($e->getMessage());
    } catch (Throwable $e) {
      throw new InvalidArgumentException($e->getMessage());
    }
  }

  /**
   * Check Have I Been Pawned if password has been breached.
   *
   * @var string $string
   * @return array
   */
  private function getHIBPData(string $string)
  {
    $breached = false;
    $count = 0;
    $error = false;

    try {
      $hash = strtoupper(sha1($string));
      $response = Http::get('https://api.pwnedpasswords.com/range/' . substr($hash, 0, 5));

      if ($response->status() != 200) {
        throw new Exception('Anrop till HIBP misslyckades.');
      }

      $hibpHashes = (string) $response->getBody();

      if ($hibpHashes) {
        $hibpHashesArray = explode("\r\n", $hibpHashes);
        foreach ($hibpHashesArray as $hibpHash) {
          if (!empty($hibpHash)) {
            list($hibpHash, $count) = explode(':', $hibpHash);

            if ($hibpHash == substr($hash, 5)) {
              $breached = true;
              $count = $count;
              break;
            }
          }
        }
      }
    } catch (Exception $e) {
      $error = 'Fel: ' . $e->getMessage();
    }

    return [
      'breached' => $breached,
      'count' => $count,
      'error' => $error,
    ];
  }

  /**
   * Calculate entropy.
   *
   * @param string $string
   * @return array
   */
  private function calculateEntropy(string $string): array
  {
    $length = strlen($string);
    $sizeOfPool = $this->getPool($string);
    $entropy = log(pow($sizeOfPool, $length), 2); // E = log2(RL)
    $score = floor(($entropy / 100) * 4);

    return [
      'value' => $entropy,
      'size_of_pool' => $sizeOfPool,
      'length' => $length,
      'score' => $score > 4 ? 4 : $score,
    ];
  }

  /**
   * Get pool, the number of unique characters from which we generate the password.
   *
   * @return int
   */
  public function getPool(string $string)
  {
    $pool = 0;

    if (preg_match("/[a-zåäö]/", $string)) {
      $pool += 29;
    }

    if (preg_match("/[A-ZÅÄÖ]/", $string)) {
      $pool += 26;
    }

    if (preg_match("/[0-9]/", $string)) {
      $pool += 10;
    }

    if (StringAnalyserHelper::hasSpecialChars($string)) {
      $pool += strlen(config('kodgeneratorn.special_chars'));
    }

    return $pool;
  }
}