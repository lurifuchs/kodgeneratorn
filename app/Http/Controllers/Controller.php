<?php

namespace App\Http\Controllers;

use App\Models\GeneratorOptions;
use App\Services\ApiService;
use App\Services\PasswordAnalyserService;
use App\Services\PasswordGeneratorService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class Controller extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  /**
   * Front page.
   *
   * @return void
   */
  public function frontpage()
  {
    $counts = DB::select("
      SELECT
      (SELECT COUNT(*) FROM adjectives LIMIT 1) AS 'adjectivesCount',
      (SELECT COUNT(*) FROM nouns LIMIT 1) AS 'nounsCount',
      (SELECT COUNT(*) FROM verbs LIMIT 1) AS 'verbsCount',
      (SELECT COUNT(*) FROM words LIMIT 1) AS 'wordsCount';
    ");

    $counts = collect($counts);
    $counts = $counts->first();
    $counts = collect($counts);

    return view('frontpage', $counts);
  }

  /**
   * Analyzs page.
   *
   * @return void
   */
  public function testYourPassword()
  {
    return view('test-your-password');
  }

  /**
   * What is a strong password page.
   *
   * @return void
   */
  public function whatIsSecurity()
  {
    return view('what-is-security');
  }

  /**
   * What is a strong password page.
   *
   * @return void
   */
  public function whatIsAStrongPassword()
  {
    return view('what-is-a-strong-password');
  }

  /**
   * How are the passwords created page.
   *
   * @return void
   */
  public function howAreThePasswordsCreated()
  {
    $counts = DB::select("
      SELECT
      (SELECT COUNT(*) FROM adjectives LIMIT 1) AS 'adjectivesCount',
      (SELECT COUNT(*) FROM nouns LIMIT 1) AS 'nounsCount',
      (SELECT COUNT(*) FROM verbs LIMIT 1) AS 'verbsCount',
      (SELECT COUNT(*) FROM words LIMIT 1) AS 'wordsCount';
    ");

    $counts = collect($counts);
    $counts = $counts->first();
    $counts = collect($counts);

    return view('how-are-the-passwords-created', $counts);
  }

  /**
   * Plugins
   *
   * @return void
   */
  public function plugins()
  {
    return view('plugins');
  }

  /**
   * Development page.
   *
   * @return void
   */
  public function development()
  {
    return view('development');
  }

  /**
   * API documenation.
   *
   * @return void
   */
  public function apiDocumentation(Request $request)
  {
    $options = new GeneratorOptions();
    $passwordGeneratorResult = App::make(PasswordGeneratorService::class)->generate($options);
    $passwordAnalysisResult = App::make(PasswordAnalyserService::class)->analyse(
      $passwordGeneratorResult->getPassword(),
      $options
    );

    $data = App::make(ApiService::class)->getV1ResponseData($passwordGeneratorResult, $passwordAnalysisResult, $options);

    return view('api-documentation', ['apiResponse' => json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT)]);
  }

}
