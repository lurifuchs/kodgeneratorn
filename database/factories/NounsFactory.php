<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class NounsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'singular_indefinite' => fake()->word(),
            'singular_definite' => fake()->word(),
            'plural_indefinite' => fake()->word(),
            'plural_definite' => fake()->word(),
        ];
    }
}
