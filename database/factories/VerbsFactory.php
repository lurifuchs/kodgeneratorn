<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class VerbsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'infinitive' => fake()->word(),
            'imperative' => fake()->word(),
            'present' => fake()->word(),
            'preterium' => fake()->word(),
            'supinum' => fake()->word(),
        ];
    }
}
