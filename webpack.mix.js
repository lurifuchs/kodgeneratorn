const mix = require('laravel-mix');
mix.disableNotifications();

// Copy files
mix.copyDirectory('resources/fonts', 'public/fonts');

// Compile JS
mix.js([
    'resources/js/menu-toggle.js',
    'resources/js/header-scroll.js',
    'resources/js/matomo.js'
], 'dist/main.js');
mix.js('resources/js/copy-password.js', 'dist');

// Minify JS
mix.minify(['public/dist/main.js', 'public/dist/copy-password.js']);

// Compile SASS
mix.sass('resources/sass/main.scss', 'dist').options({
    processCssUrls: false
})
.version();
